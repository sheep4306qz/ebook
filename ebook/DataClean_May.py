import configparser
import pymysql
import pandas as pd # 引用套件並縮寫為 pd

df = pd.read_csv('/home/book/Desktop/ebook/project_ebook/DataSet/national-library-board-malay-ebooks-2019-08-23T01-01-15Z.csv')  
config=configparser.ConfigParser()

# 打開資料庫連接
db = pymysql.connect("localhost","root","","ebook" )
 
# 使用 cursor() 方法創建一個游標對象 cursor
cursor = db.cursor()

for i in range(1000):
    # SQL 插入語句
    sql = "INSERT INTO book(bookname, languages, author, summary, url, image) VALUES ('%s', '%s',    '%s',  '%s', '%s', '%s')" % (pymysql.escape_string(df['book_title'].iloc[i]), df['language'].iloc[i],  pymysql.escape_string(df['author_name'].iloc[i]), pymysql.escape_string(df['summary'].iloc[i]),pymysql.escape_string(df['resource_url'].iloc[i]),pymysql.escape_string(df['cover'].iloc[i]))

    # 執行sql語句
    cursor.execute(sql)
    # 執行sql語句
    db.commit()
 

#關閉連接
db.close()