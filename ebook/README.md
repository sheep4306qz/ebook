# ebook
ebook restful api

註冊 :

post /signUp
{"email" : email, 
 "pwd" : pwd,
 "name" : name,
 "ddl_year": ddl_year,
 "ddl_month":ddl_month,
 "ddl_day":ddl_day,
 "sex" : sex
}
{"status" : true/fail}

登入 :

post /login
{"email" : email,
 "pwd" : pwd,
}
{"status" : true

or

"status" : fail,
"note" : true/fail
}

排行榜Top5 : 

get /Top5
{x}
{ "bid" : bid, 
 "bName" : bName,
 "author" : author,
 "picture" : picture
}

近期上架 : 

get /newBooks
{x}
{ "bid" : bid, 
 "bName" : bName,
 "author" : author,
 "picture" : picture
}

全部書5 : 

get /AllBook5
{x}
{ "bid" : bid, 
 "bName" : bName,
 "author" : author,
 "picture" : picture
}

全部書48 : 

get /AllBook48
{"page" : page}
{"bid" : bid, 
 "bName" : bName,
 "author" : author,
 "picture" : picture,
 "PageCount" : PageCount
}


馬來文5 :

get /May5
{x}
{ "bid" : bid, 
 "bName" : bName,
 "author" : author,
 "picture" : picture
}

泰文5 :

get /Thai5
{x}
{"bid" : bid, 
 "bName" : bName,
 "author" : author,
 "picture" : picture
}

查詢使用者資料

get /getUserAll
{x}
{"email" : email, 
 "ddl_year":ddl_year,
 "ddl_month":ddl_month,
 "ddl_day":ddl_day,
 "name" : name,
 "sex" : sex
}

修改使用者資料

post /modifyUserAll
{
 "pwd" : pwd,
 "name" : name,
}
{"status" : true/fail
 "name" : name
}

評論頁面 :

get /Rew
{x}
{"bid" : bid, 
"picture" : picture,
"bName" : bName,
"rTime" : rTime,
"author" : author,
"text" : text
}

借閱紀錄畫面 : 

get /Rec
{x}
{
"bid" : bid, 
"picture" : picture,
"bName" : bName,
"Time" : Time,
"author" : author,
"Rechistory" : true/fail(是否評論過)
}

送出評論 :

post /sendRew
{
"text" : text
"bid" : bid
}
{"status" : true/fail}

書籍介紹 :

get /bookIntro1
{"bid" : bid}
{"picture" : picture,
 "lang" : lang,
"bName" : bName,
"author" : author,
"summary" : summary
}

書籍介紹(評論) :

get /bookIntro2
{"bid" : bid}
{
"text" : text,
"email" : email
"status" : true/false (沒有評論為false)
}



查詢書名 : 

get /findBook
{"bName" : bName}
{"status" : true/false,
"note" : true/false,
"picture" : picture,
 "lang" : lang,
"bName" : bName,
"author" : author,
"bid" : bid
}

查詢我的書庫 :

get /myBook
{x}
{"bid" : bid,
 "bName" : bName,
 "borTime" : borTime,
 "Timeout" : Timeout,
 "lang" : lang,
 "url" : url,
 "author" : author,
 "picture" , picture,
 "borbook" : borbook(還有幾本可以藉)
}

Read :

post /readBook
{"bid" : bid}
{"url" : url}

Return :

post /returnBook
{"bid" : bid}
{"status" : true/fail}

我的蒐藏:

post /myCollect
{x}
{"picture" : picture,
 "bName" : bName,
 "author" : author,
 "bid" : bid
}

蒐藏按鈕:

post /collectBottom
{"bid" : bid}
{"status" : true/fail}

借書:

post /borrow
{"bid":bid}
{"status":true/fail}
 



