from flask import Flask, render_template, flash, redirect, request, session, logging, url_for, jsonify
from flask_sqlalchemy import SQLAlchemy
from  sqlalchemy.sql.expression import func
from sqlalchemy import text
import configparser
from datetime import timedelta
from flask_login import LoginManager,current_user,login_user,logout_user

import datetime
import pymysql
import random
from flask_cors import CORS
#12/30更新的部分 後台+數據化圖示
#from flask_admin import Admin
#from flask_admin.contrib.sqla import ModelView
import pygal
import urllib.parse

import os
from flask import Flask, url_for, redirect, render_template, request
from flask_sqlalchemy import SQLAlchemy
from wtforms import form, fields, validators
import flask_admin as admin
from flask_admin.contrib import sqla
from flask_admin import helpers, expose
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
CORS(app)
db = SQLAlchemy(app)
#admin = Admin(app)
config = configparser.ConfigParser()  
config.read('data.ini')
#設置靜態文件緩存過期時間
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = timedelta(seconds=1)
password=config['DEFAULT']['password']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:'+password+'@localhost:3306/ebook'
app.config['SECRET_KEY'] = '!9m@S-dThyIlW[pHQbN^'
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=1)

conn = pymysql.connect( host = 'localhost' ,  user ='root' ,  passwd =password , db = "ebook")  


           
class user(db.Model):
    __tablename__='user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15))
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(256))
    birth = db.Column(db.String(15))
    sex =db.Column(db.String(10))
    def __init__(self,username,email,password,birth,sex):
        self.username=username
        self.email=email
        self.password=password
        self.birth=birth
        self.sex=sex

#育全定義的部分，需再考慮多對多與外來鍵關係，連帶考慮因此會多新增table的操作
class book(db.Model):
    __tablename__='book'
    id = db.Column(db.Integer, primary_key=True)
    bookname = db.Column(db.String(100))
    languages = db.Column(db.String(10))
    #published = db.Column(db.String(15))
    author = db.Column(db.String(100))
    summary = db.Column(db.Text)
    url  = db.Column(db.String(200))
    image = db.Column(db.Text)
    addedTime = db.Column(db.TIMESTAMP,server_default=db.func.now())
    counts = db.Column(db.Integer,server_default=text('0'))
    # db.relationship("user", secondary=collect, backref="Book")
    # db.relationship("user", secondary=review, backref="Book")
    def __init__(self,bookname,languages,author,summary,url,image):
        self.bookname=bookname
        self.languages=languages
        self.author=author
        self.summary=summary
        self.url=url
        self.image=image

class borrowbook(db.Model):
    __tablename__='borrowbook'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50))
    book_id = db.Column(db.String(50))
    borrowTime = db.Column(db.TIMESTAMP, server_default=db.func.now())
    status = db.Column(db.Integer,server_default=text('1')) #1為可閱讀，0為不可閱讀與歷史紀錄
    def __init__(self,email,book_id):
        self.email=email
        self.book_id=book_id

#多對多
class collect(db.Model):
    __tablename__='collect'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50))
    book_id = db.Column(db.String(50))
    collectTime = db.Column(db.TIMESTAMP,server_default=db.func.now())
    def __init__(self,email,book_id):
        self.email=email
        self.book_id=book_id

#多對多 
class review(db.Model):
    __tablename__='review'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50))
    book_id = db.Column(db.String(50))
    content = db.Column(db.Text)
    reviewTime = db.Column(db.TIMESTAMP,server_default=db.func.now())  
    def __init__(self,email,book_id,content):
        self.email=email
        self.book_id=book_id
        self.content=content

class Admin(db.Model):
    __tablename__='Admin'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(80))
    password = db.Column(db.String(256))
    def __init__(self,login,password):
            self.login=login
            self.password=password

    # Flask-Login integration
    # NOTE: is_authenticated, is_active, and is_anonymous
    # are methods in Flask-Login < 0.3.0
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username

# Define login and registration forms (for flask-login)
class LoginForm(form.Form):
    login = fields.StringField(validators=[validators.required()])
    password = fields.PasswordField(validators=[validators.required()])

    def validate_login(self, field):
        user = self.get_user()

        if user is None:
            raise validators.ValidationError('Invalid user')

        # we're comparing the plaintext pw with the the hash from the db
        if not check_password_hash(user.password, self.password.data):
        # to compare plain text passwords use
        # if user.password != self.password.data:
            raise validators.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(Admin).filter_by(login=self.login.data).first()


class RegistrationForm(form.Form):
    login = fields.StringField(validators=[validators.required()])
    email = fields.StringField()
    password = fields.PasswordField(validators=[validators.required()])

    def validate_login(self, field):
        if db.session.query(Admin).filter_by(login=self.login.data).count() > 0:
            raise validators.ValidationError('Duplicate username')



# Initialize flask-login
def init_login():
    login_manager = LoginManager()
    login_manager.init_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(Admin).get(user_id)


# Create customized model view class
class MyModelView(sqla.ModelView):

    def is_accessible(self):
        return current_user.is_authenticated


# Create customized index view class that handles login & registration
class MyAdminIndexView(admin.AdminIndexView):

    @expose('/')
    def index(self):
        if not current_user.is_authenticated:
            return redirect(url_for('.login_view'))
        return super(MyAdminIndexView, self).index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        # handle user login
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login_user(user)

        if current_user.is_authenticated:
            return redirect(url_for('.index'))
        
        self._template_args['form'] = form
        return super(MyAdminIndexView, self).index()
    @expose('/register/', methods=('GET', 'POST'))
    def register_view(self):
        form = RegistrationForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = Admin()

            form.populate_obj(user)
            # we hash the users password to avoid saving it as plaintext in the db,
            # remove to use plain text:
            user.password = generate_password_hash(form.password.data)

            db.session.add(user)
            db.session.commit()

            login_user(user)
            return redirect(url_for('.index'))
        link = '<p>Already have an account? <a href="' + url_for('.login_view') + '">Click here to log in.</a></p>'
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(MyAdminIndexView, self).index()
 
    @expose('/logout/')
    def logout_view(self):
        logout_user()
        return redirect(url_for('.index'))
# Initialize flask-login
init_login()

# Create admin
admin = admin.Admin(app, 'Example: Auth', index_view=MyAdminIndexView(), base_template='my_master.html')
#畫圖後台 118~192 route('/analysis_count')、route('/analysis_gender')、route('/analysis_languages')
admin.add_view(MyModelView(book, db.session))
admin.add_view(MyModelView(user, db.session))
admin.add_view(MyModelView(borrowbook, db.session))
admin.add_view(MyModelView(collect, db.session))
admin.add_view(MyModelView(review, db.session))

@app.route('/analysis_count')
def analysis_count():
    cursor = conn.cursor()
    sql =  "select bookname, counts from book where counts>0 order by counts DESC;"
    cursor.execute(sql)
    result = cursor.fetchall()
    bookname = []
    bookcounts = []
    for i in range(len(result)):
        bookname.append(result[i][0]) 
        bookcounts.append(result[i][1])
    bar_chart = pygal.Bar()
    bar_chart.title = 'bookname and borrow of counts'
    bar_chart.x_labels = bookname
    bar_chart.add('bookcounts', bookcounts)
    cursor.close()
    return render_template('graph.html', chart=bar_chart)

@app.route('/analysis_gender')
def analysis_gender():
    cursor = conn.cursor()
    sql =  "select sex from user;"
    cursor.execute(sql)
    result = cursor.fetchall()
    boy = []
    girl = []
    for i in range(len(result)):
        if result[i][0] == '男':
            temp = 1
            boy.append(temp) 
        elif result[i][0] == '女':
            temp = 0
            girl.append(temp)
    pie_chart =  pygal.Pie(print_values=True)
    pie_chart.title = 'gender counts'
    per_boy=len(boy)/(len(boy)+len(girl))*100
    per_girl=len(girl)/(len(boy)+len(girl))*100
    pie_chart.add('boy', per_boy,formatter=lambda x: '%.2f%%'% per_boy)
    pie_chart.add('girl',per_girl ,formatter=lambda x: '%.2f%%'% per_girl)
    pie_chart.render()
    cursor.close()
    return render_template('graph.html', chart=pie_chart)

@app.route('/analysis_languages')
def analysis_languages():
    cursor = conn.cursor()
    sql =  "select languages from book;"
    cursor.execute(sql)
    result = cursor.fetchall()
    may = []
    Thai = []
    for i in range(len(result)):
        if result[i][0] == 'may':
            temp = 1
            may.append(temp) 
        elif result[i][0] == 'Thai':
            temp = 0
            Thai.append(temp)
    cursor.close()
    pie_chart =  pygal.Pie(print_values=True)
    pie_chart.title = 'gender counts'
    per_thai=len(Thai)/(len(may)+len(Thai))*100
    per_may=len(may)/(len(may)+len(Thai))*100
    pie_chart.add('may', per_thai,formatter=lambda x: '%.2f%%'% per_thai)
    pie_chart.add('Thai',per_may ,formatter=lambda x: '%.2f%%'% per_may)
    pie_chart.render()
    cursor.close()
    return render_template('graph.html', chart=pie_chart)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/Bookintro')
def Bookintro():
    return render_template('bookintro.html')

@app.route('/AllBook')
def AllBook():
    return render_template('AllBook48.html')

@app.route('/Member_login')
def Member_login():
    return render_template('login.html')

@app.route('/Member_Register')
def Register():
    return render_template('register.html')

@app.route('/MyCollection')
def MyCollection():
    return render_template('Collect.html')

@app.route('/MemberCenter')
def MemberCenter():
    return render_template('MemberCenter.html')

@app.route('/changeMember')
def changeMember():
    return render_template('changeMember.html')

@app.route('/MemberCenter_Comment')
def MemberCenter_Comment():
    return render_template('MemberCenter_Comment.html')

@app.route('/MemberCenter_Borrowed')
def MemberCenter_Borrowed():
    return render_template('MemberCenter_Borrowed.html')

@app.route('/MyBookList')
def MyBook():
    return render_template('MyBook.html')

@app.route('/Collect')
def Collect():
    return render_template('Collect.html')

@app.route('/search')
def search():
    return render_template('search.html')

@app.route('/AboutUs')
def AboutUs():
    return render_template('AboutUs.html')

@app.route('/More_Q&A')
def MoreQA():
    return render_template('More_Q&A.html')

@app.route('/Contact')
def Contact():
    return render_template('Contact.html')

@app.route('/Logout')
def Logout():
    # Removing data from session by setting logged_flag to False.
    session['logged_in'] = False
    session.clear()
    # redirecting to home page
    return redirect(url_for('home'))
'''
'''
#育全寫的API 
#RestfulAPI
@app.route('/Top5', methods=['POST', 'GET'])  
def Top5():
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    sql = "select id,bookname, author, image from book where counts>0 order by counts DESC limit 5 ;"
    #sql = "select {0}, author, image from book where counts>0 order by counts DESC limit 5 ;".format(bookname)
    cursor.execute(sql)
    result = cursor.fetchall()
    print(result)
    # listbookname = []
    # listauthor = []
    # listimage = []
    # for i in range(0,5):
    #     listbookname.append(result[i][0])
    #     listauthor.append(result[i][1])
    #     listimage.append(result[i][2])
    # #print(listbookname)
    # data = {
    #     'author': listauthor,
    #     'bName': listbookname,
    #     'picture': listimage
    # }
    lists = []
    for i in range(0,5):
        lists.append({'bid':result[i][0],'bName': result[i][1], 'author': result[i][2], 'picture': result[i][3]})
    #print(lists)
    cursor.close()
    return jsonify(lists)

#get /AllBook5 {x} {"bName" : bName, "author" : author, "picture" : picture }
@app.route('/AllBook5', methods=['POST', 'GET'])  
def AllBook5():
    query_all5 = book.query.filter_by().order_by(func.random()).limit(5).all()
    #print(query_may)
    #print(total_item)
    # listbookname = []
    # listauthor = []
    # listimage = []
    # for i in total_item:
    #     listbookname.append(query_may[i].bookname)
    #     listauthor.append(query_may[i].author)
    #     listimage.append(query_may[i].image)
    # print(listbookname)
    # data = {'bName': listbookname,
    #         'author': listauthor,
    #         'picture': listimage
    #         }
    lists = []
    for i in range(len(query_all5)):
        lists.append({'bid':query_all5[i].id,'bName': query_all5[i].bookname, 'author': query_all5[i].author, 'picture': query_all5[i].image})
    #print(lists)        
    return jsonify(lists)

#get /AllBook50 {"page" : page} {"bName" : bName, "author" : author, "picture" : picture }
@app.route('/AllBook48', methods=['POST', 'GET'])  
def AllBook48():
    pagedata = request.values.get('page')
    # print("pagedata type: ", type(pagedata))
    # print("pagedata value: ", pagedata)
    page = int(pagedata)
    query_allbook48 = book.query.order_by(book.addedTime.desc()).limit(48).offset((page-1)*48).all()
    # listbookname = []
    # listauthor = []
    # listimage = []
    # for i in range(0,50):
    #     listbookname.append(query_allbook50[i].bookname)
    #     listauthor.append(query_allbook50[i].author)
    #     listimage.append(query_allbook50[i].image)
    # #print(listbookname)
    # data = {'page': page,
    #         'bName': listbookname,
    #         'author': listauthor,
    #         'picture': listimage
    #         }
    # print(data)
    lists = []
    for i in range(0,48):  
        lists.append({'bid':query_allbook48[i].id,'page': page, 'bName': query_allbook48[i].bookname, 'author': query_allbook48[i].author, 'picture': query_allbook48[i].image})
    return jsonify(lists)

#get /May5 {x} {"bName" : bName, "author" : author, "picture" : picture }
@app.route('/May5', methods=['POST','GET'])  
def May5(): 
    query_may = book.query.filter_by(languages='may').order_by(func.random()).limit(5).all()
    #print(query_may)
    #print(total_item)
    # listbookname = []
    # listauthor = []
    # listimage = []
    # for i in total_item:
    #     listbookname.append(query_may[i].bookname)
    #     listauthor.append(query_may[i].author)
    #     listimage.append(query_may[i].image)
    # print(listbookname)
    # data = {'bName': listbookname,
    #         'author': listauthor,
    #         'picture': listimage
    #         }
    lists = []
    for i in range(len(query_may)):
        lists.append({'bid':query_may[i].id,'bName': query_may[i].bookname, 'author': query_may[i].author, 'picture': query_may[i].image})
    #print(lists)        
    return jsonify(lists)
    #return jsonify(data)

#Thai5 {x} {"bName" : bName, "author" : author, "picture" : picture }
@app.route('/Thai5', methods=['POST','GET'])  
def Thai5():
    query_thai = book.query.filter_by(languages='Thai').order_by(func.random()).limit(5).all()
    #print(query_may)
    #print(total_item)
    # listbookname = []
    # listauthor = []
    # listimage = []
    # for i in total_item:
    #     listbookname.append(query_may[i].bookname)
    #     listauthor.append(query_may[i].author)
    #     listimage.append(query_may[i].image)
    # print(listbookname)
    # data = {'bName': listbookname,
    #         'author': listauthor,
    #         'picture': listimage
    #         }
    lists = []
    for i in range(len(query_thai)):
        lists.append({'bid':query_thai[i].id,'bName': query_thai[i].bookname, 'author': query_thai[i].author, 'picture': query_thai[i].image})
    #print(lists)        
    return jsonify(lists)
    #return jsonify(data)

#查詢書名 :
#findBook {"bName" : bName} {"picture" : picture, "lang" : lang, "bName" : bName, "author" : author, "bid" : bid }  
@app.route('/findBook', methods=['POST','GET'])  
def findBook():
    dataget = urllib.parse.unquote(request.values.get('bName'))
    #模糊搜尋回傳符合的前30筆
    if dataget=='':
        return jsonify({'status':False,'note':True})
    else:
        query_findbook = book.query.filter(book.bookname.like("%"+dataget+"%")if dataget is not None else "").limit(30).all()
        if query_findbook !=[]:
            lists = []
            for i in range(len(query_findbook)):
                lists.append({'status':True,'bid': query_findbook[i].id,'author': query_findbook[i].author,'bName': query_findbook[i].bookname,'picture': query_findbook[i].image,'lang': query_findbook[i].languages})
            return jsonify(lists)
        else:
            return jsonify({'status':False,'note':False})

#我寫的API 
#RestfulAPI
@app.route('/getUserAll',methods = ['GET'])
def getUserAll():
    #email=request.values.get('email') #使用前端儲存的localStorage
    #UserAll=user.query.filter_by(email = email).first() #這邊使用session抓取
    #UserAll=user.query.filter_by(email = 'test@gmail.com').first()
    UserAll=user.query.filter_by(email = session['email']).first() #這邊使用session抓取
    bir=datetime.datetime.strptime(UserAll.birth, "%Y/%m/%d")
    ddl_year=bir.strftime("%Y")
    ddl_month=bir.strftime("%m")
    ddl_day=bir.strftime("%d")
    #UserAll=user.query.filter_by(email = 'test@gmail.com').first()
    #UserAll=user.query.filter_by(email = session['email']).first() #這邊使用session抓取
    #return jsonify({'email':UserAll.email,'bir':UserAll.birth,'name':UserAll.username,'sex':UserAll.sex})
    return jsonify({'email':UserAll.email,'ddl_year':ddl_year,'ddl_month':ddl_month,'ddl_day':ddl_day,'name':UserAll.username,'sex':UserAll.sex})

@app.route('/modifyUserAll',methods = ['POST'])
def modifyUserAll():
    #email=request.values.get('email') #使用前端儲存的localStorage
    #Username=request.form.get('name')
    #Userpwd=request.form.get('pwd')
    Username=request.values.get('name')
    Userpwd=request.values.get('pwd')
    #UserAll=user.query.filter_by(email = email).first() #這邊使用session抓取
    #UserAll=user.query.filter_by(email = 'test@gmail.com').first() #這邊使用session抓取
    UserAll=user.query.filter_by(email = session['email']).first()
    if UserAll.username != Username:
        UserAll.username = Username
    if Userpwd!='':
        UserAll.password=generate_password_hash(Userpwd, method='sha256',salt_length=8)
        db.session.commit()
        return jsonify({'status':True,'name':UserAll.username})
    else:
        db.session.commit()
        return jsonify({'status':True,'name':UserAll.username})

@app.route('/Rew',methods = ['GET'])
def Rew():
    #email=request.values.get('email') #使用前端儲存的localStorage
    #Rew=review.query.filter_by(email = email).order_by(review.reviewTime.desc()).all()
    #Rew=review.query.filter_by(email = 'test@gmail.com').order_by(review.reviewTime.desc()).all()
    Rew=review.query.filter_by(email = session['email']).order_by(review.reviewTime.desc()).all() #這邊使用session抓取
    RewBook=[]
    for i in Rew:
        Book=book.query.filter_by(id = i.book_id).first()      
        RewBook.append({'bid':Book.id,'picture':Book.image,'bName':Book.bookname,'author':Book.author,'rTime':i.reviewTime,'text':i.content})
    return jsonify(RewBook)

@app.route('/Rec',methods = ['GET'])
def Rec():
    #email=request.values.get('email') #使用前端儲存的localStorage
    #Rec=borrowbook.query.filter_by(email = email).order_by(borrowbook.borrowTime.desc()).all()
    #Rec=borrowbook.query.filter_by(email = 'test@gmail.com').order_by(borrowbook.borrowTime.desc()).all()
    Rec=borrowbook.query.filter_by(email = session['email']).order_by(borrowbook.borrowTime.desc()).all()
    RecBook=[]
    for i in Rec:
        Rew=review.query.filter_by(book_id=i.book_id,email=i.email).first() #查詢使用者是否有評論過某書紀錄於review中
        if Rew ==None:
            Rechistory=False #此使用者沒有評論過此書
        else:
            Rechistory=True #此使用者評論過此書
        Book=book.query.filter_by(id = i.book_id).first()
        RecBook.append({'bid':Book.id,'picture':Book.image,'bName':Book.bookname,'author':Book.author,'Time':i.borrowTime,'Rechistory':Rechistory})
    return jsonify(RecBook)

@app.route('/sendRew',methods = ['POST'])
def sendRew():
    #email=request.values.get('email') #使用前端儲存的localStorage
    #rewtext=request.form.get('text')
    #bid=request.form.get('bid')
    rewtext=request.values.get('text')
    bid=request.values.get('bid')
    #Add data to review table
    try:
        #sendRew=review(email,bid,rewtext)
        sendRew=review(session['email'],bid,rewtext) #這邊使用session抓取
        #sendRew=review('test@gmail.com',bid,rewtext)
        db.session.add(sendRew)
        db.session.commit()
        return jsonify({'status':True})
    except:
        return jsonify({'status':False})

@app.route('/signUp',methods = ['POST'])
def signUp():
    email=request.form.get('email')
    pwd=request.form.get('pwd')
    name=request.form.get('name')
    ddl_year=request.form.get('ddl_year')
    ddl_month=request.form.get('ddl_month')
    ddl_day=request.form.get('ddl_day')
    bir=str(ddl_year)+'/'+str(ddl_month)+'/'+str(ddl_day)
    gender=request.form.get('gender')

    # Cheking that method is post and form is valid or not.
    try:
        check=user.query.filter_by(email=email).count()
        #print(check)
        if  check == 0:
            # if all is fine, generate hashed password
            hashed_password = generate_password_hash(pwd, method='sha256',salt_length=8)
            # create new user model object
            new_user = user(
                username = name, 
                email = email, 
                password = hashed_password,
                birth=bir,
                sex=gender
                 )
            # saving user object into data base with hashed password
            db.session.add(new_user)
            db.session.commit()
            flash('You have successfully registered', 'success')
            # if registration successful, then redirecting to login Api
            return jsonify({'status':True})
        elif    check !=0:
            return jsonify({'status':False,'note':False})
    except:
            return jsonify({'status':False,'note':True})

@app.route('/login',methods = ['POST'])
def login():
    email=request.form.get('email')
    pwd=request.form.get('pwd')
    usertable = user.query.filter_by(email = email).first()
    if usertable:
        # if user exist in database than we will compare our database hased password and password come from login form 
        if check_password_hash(usertable.password, pwd):
            # if password is matched, allow user to access and save email and username inside the session
            flash('You have successfully logged in.', "success")
            session['logged_in'] = True
            session['email'] = usertable.email 
            session['username'] = usertable.username
            # After successful login, redirecting to home page
            print(session['email'])
            return jsonify({'status':True})
        else:
            # if password is in correct , redirect to login page
            flash('Username or Password Incorrect', "Danger")
            
            return jsonify({'status':False,'note':False})
    else:
        return jsonify({'status':False,'note':True})

@app.route('/logout')
def logout():
    # Removing data from session by setting logged_flag to False.
    session['logged_in'] = False
    session.clear()
    # redirecting to home page
    return redirect(url_for('home'))
    #return jsonify({'status':True})

#丁丁寫的API 
#RestfulAPI
@app.route('/readBook',methods = ['GET', 'POST'])
def readBook():
    getBid = request.values.get('bid')
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    sql = 'select url from book where id = {}'.format(getBid)
    cursor.execute(sql)
    result = cursor.fetchall()

    data = {
        'url' : result[0] 
    }
    cursor.close()
    return jsonify(data)

@app.route('/newBook',methods = ['GET', 'POST'])
def newBook():
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    sql = 'select id,bookname, author, image from book order by addedTime DESC'
    cursor.execute(sql)
    result = cursor.fetchall()
    
    lists = []
    #0~5
    for i in range(0,5):
        lists.append({'bid':result[i][0],'bName' : result[i][1], 'author' : result[i][2], 'picture' : result[i][3]})
    cursor.close()
    return jsonify(lists)

@app.route('/bookIntro1',methods = ['GET', 'POST'])
def bookIntro1():
    getBid = request.values.get('bid')
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    sql = 'select image, languages, bookName, author, summary from book where id = {}'.format(getBid)
    cursor.execute(sql)
    result = cursor.fetchall()
    data = {
        'picture' : result[0][0],
        'lang' : result[0][1],
        'bName' : result[0][2],
        'author' : result[0][3],
        'summary' : result[0][4]
    }
    cursor.close()
    return jsonify(data)

@app.route('/bookIntro2',methods = ['GET', 'POST'])
def bookIntro2():
    getBid = request.values.get('bid')
    print(type(getBid))
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    sql = 'select content, email from review where book_id = {}'.format(getBid)
    cursor.execute(sql)
    rows_count = cursor.execute(sql)
    result = cursor.fetchall()
    print(result)
    lists = []
    if rows_count !=0:
        for i in range(0,len(result)):
            lists.append({'status':True,'text' : result[i][0], 'email' : result[i][1]})
        cursor.close()
        return jsonify(lists)
    else:
        data={
            'status':False
            }
        cursor.close()
        return jsonify(data)

@app.route('/returnBook',methods = ['GET', 'POST'])
def returnBook():
    # get session
    email = session['email']
    #email = 'test@gmail.com'
    #email=request.values.get('email') #使用前端儲存的localStorage
    getBid = request.values.get('bid')
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    sql = "UPDATE borrowbook SET status = 0 WHERE book_id = '%d' And email = '%s' "%(int(getBid),email)
    cursor.execute(sql)
    conn.commit()
    data = {
        "status" : True
    }
    cursor.close()
    return jsonify(data)

@app.route('/myBook',methods = ['GET', 'POST'])
def mybook():
    # get session
    email = session['email']
    #email = 'test@gmail.com'
    #email=request.values.get('email') #使用前端儲存的localStorage
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    sql = "select * from borrowbook where email = '%s' and status = 1"%(email)
    cursor.execute(sql)
    rows_count = cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    listbook=[]
    lists=[]
    borbook = 5-rows_count #還有幾本書可以借
    NowTime = datetime.datetime.now() #系統現在時間
    for i in range(0,len(result)):
        if (NowTime-result[i][3]).days>=14:
            cursor = conn.cursor()
            conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
            sql = "UPDATE borrowbook SET status = 0 WHERE book_id = '%s' And email = '%s' "% (result[i][2],result[i][1])
            cursor.execute(sql)
            conn.commit()
            cursor.close()
        else:
            listbook.append(result[i][2])
            lists.append({'borTime':result[i][3],'borbook':borbook})
    
    for i in range(len(listbook)):
        cursor = conn.cursor()
        conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
        sql = 'select id, bookname, languages, author, url, image from book where id = {}'.format(listbook[i])
        cursor.execute(sql)
        result = cursor.fetchall()
        lists[i]['bid']=result[0][0]
        lists[i]['bName']=result[0][1]
        borTime=lists[i]['borTime']
        Timeout = borTime + datetime.timedelta(days = 14)
        lists[i]['Timeout']=Timeout
        lists[i]['lang']=result[0][2]
        lists[i]['author']=result[0][3]
        lists[i]['url']=result[0][4]
        lists[i]['picture']=result[0][5]
        cursor.close()
    cursor.close()
    return jsonify(lists)

@app.route('/myCollect',methods = ['GET', 'POST'])
def myCollect():
    #email=request.values.get('email') #使用前端儲存的localStorage
    email = session['email']
    #email = 'test@gmail.com'
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    # subquery 不確定是不是這樣format
    sql = "select image, bookname, author, id from book where book.id = any(select book_id from collect where email = '%s')"%(email)
    cursor.execute(sql)
    rows_count = cursor.execute(sql)
    result = cursor.fetchall()
    lists = []
    for i in range(rows_count):
        lists.append({'picture' : result[i][0], 'bName' : result[i][1], 'author' : result[i][2], 'bid' : result[i][3]})
    cursor.close()
    return jsonify(lists)

@app.route('/collectBottom',methods = ['GET', 'POST'])
def collectBottom():
    #email=request.values.get('email') #使用前端儲存的localStorage
    email = session['email']
    #email = 'test@gmail.com'
    getBid = request.values.get('bid')
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    sql = "select * from collect where book_id ='%d' and email='%s'"%(int(getBid),email)
    cursor.execute(sql)
    rows_count = cursor.execute(sql)
    cursor.close()
    if rows_count==0:
        cursor = conn.cursor()
        conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
        # subquery 不確定是不是這樣format
        sql = "insert into collect(email, book_id) values('%s','%d')"% (email,int(getBid))
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        data = {
            "status" : True
        }
        return jsonify(data)
    else:
        cursor = conn.cursor()
        conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
        # subquery 不確定是不是這樣format
        sql = "DELETE FROM collect WHERE email='%s' and book_id='%d'"% (email,int(getBid))
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        data = {
            "status" : False
        }
        return jsonify(data)

@app.route('/borrow',methods = ['GET', 'POST'])
def borrow():
    #email=request.values.get('email') #使用前端儲存的localStorage
    email = session['email']
    print(email)
    #email = 'test@gmail.com'
    getBid = request.values.get('bid')
    print(getBid)
    cursor = conn.cursor()
    conn.ping(reconnect=True) #提前ping一下 確認資料庫連接
    #sql1 確認有沒有這個人
    #sql1 = "select * from user where email='%s'"%(email)
    #rows_count_user = cursor.execute(sql1)
    rows_count_user=user.query.filter_by(email=email).count()
    #print(rows_count_user)
    if rows_count_user !=0:
        #sql2 確認這個存在的人有沒有正在借著這本書
        sql2 = "select * from borrowbook where email='%s' and book_id='%d' and status=1"%(email,int(getBid))
        rows_count_borrowbook = cursor.execute(sql2)
        if rows_count_borrowbook ==0:
            sql3 = "select * from borrowbook where email='%s' and status=1"%(email)
            rows_count = cursor.execute(sql3)
            if rows_count<5: #借閱書本數量已達最高值5，不能再借書
                #sql4 借書
                sql4 = "insert into borrowbook(email, book_id) values('%s','%d')"% (email,int(getBid))
                cursor.execute(sql4)
                conn.commit()
                #sql5 書被借出的次數增加
                sql5 = "UPDATE book SET counts = counts+1 WHERE id = '%d' "%(int(getBid))
                cursor.execute(sql5)
                conn.commit()
                cursor.close()
                data = {
                    "status" : True
                }
                return jsonify(data)
            else:
                #借閱數量已達最大值
                data = {
                    "status" : False
                }
                return jsonify(data)
        else:
            #已借閱此書
            data = {
                "status" : False
            }
            return jsonify(data)
    else:
        #使用者不存在
        data = {
            "status" : False
        }
        return jsonify(data)

def build_sample_db():
    """
    Populate a small db with some example entries.
    """

    import string
    import random

    db.create_all()
    # passwords are hashed, to use plaintext passwords instead:
    # test_user = User(login="test", password="test")
    test_user = Admin(login="test109423043test", password=generate_password_hash("109423043test109423043"))
    db.session.add(test_user)
    db.session.commit()
    return

if __name__ == '__main__':
    # Creating database tables
    # running server
    build_sample_db()
    app.config['JSON_AS_ASCII'] = False
    #app.run(host="0.0.0.0",debug=True)
    app.run(host="0.0.0.0",debug=False)