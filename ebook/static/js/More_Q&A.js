function openCity(evt, qa) {
    var i, qacontent, qalinks;
    qacontent = document.getElementsByClassName("qacontent");
    for (i = 0; i < qacontent.length; i++) {
        qacontent[i].style.display = "none";
    }
    qalinks = document.getElementsByClassName("qalinks");
    for (i = 0; i < qalinks.length; i++) {
        qalinks[i].className = qalinks[i].className.replace(" active", "");
    }
    document.getElementById(qa).style.display = "block";
    evt.currentTarget.className += " active";
}
      
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
