$(document).ready(function(){
  $.ajax({
  url: '/getUserAll',
  dataType: "json",
  type: "get",
  async: false,
  success: function(data){
    // console.log(data);
    
    // console.log(data.email);
    
    // for (var i=0; i<data.length;i++){
    //   var info = data[i];
      var html ="<form name='form'><h5 class='card-title' style='color: #577C8A'>Modify Member Detail</h5><br><h6 class='card-subtitle mb-2 text-muted' > NAME<input type='text' name='username' value='"
      +data.name+
      "' ></h6><p id='testName'></p><h6 class='card-subtitle mb-2 text-muted'>BIRTH："
      +data.ddl_year+
      "/"
      +data.ddl_month+
      "/"
      +data.ddl_day+
      "</h6></div><h6 class='card-subtitle mb-2 text-muted'>GENDER："
      +data.sex+"</h6><h6 class='card-subtitle mb-2 text-muted'>EMAIL:"
      +data.email+
      "</h6><h6 class='card-subtitle mb-2 text-muted' >PASSWORD：<input type=password name='pwd'></h6><p id='testPassword'></p><h6 class='card-subtitle mb-2 text-muted' >VERIFY PASSWORD：<input type=password name='confirm'></h6><p id='testRepassword'></p><input type='button' class='modify'  onclick='submitNewMember()' value='Modify'></form>";

                  
    // }
    $("#memberbox").html(html);
  }
})


})


function validate_Lengthuser(minLegth,maxlength,inputFiled)
    {
    if(inputFiled.length<minLegth||inputFiled.length>maxlength)
    {
        
        document.getElementById("testName").innerHTML="<font color='red'>* Name shall be 3-15 characters!</font>"
        $("[name='username']").focus(); //focus那個input
        return false;
    }else{
        return true;
    }
    }



function validate_Lengthpwd(minLegth,maxlength,inputFiled)
    {
    if(inputFiled.replace(/(^s*)|(s*$)/g, "").length ==0){
        return true;
    }
    else if(inputFiled.length<minLegth||inputFiled.length>maxlength)
    {
        document.getElementById("testPassword").innerHTML="<font color='red'>* Password shall be 6-20 characters!</font>"
        $("[name='pwd']").focus(); //focus那個input
        return false;
    }
    else{
        return true;
    }
    }

    // password 驗證
    function validatepwd(pwd,confirm,checkLengthpwd) {
    if(pwd != confirm && checkLengthpwd==true)
    {
        document.getElementById("testRepassword").innerHTML="<font color='red'>* Different from new password!</font>"
        $("[name='confirm']").focus(); //focus那個input
        return false;
    }
    else {
        return true;
    }
    }

function submitNewMember(){

    document.getElementById("testName").innerHTML="";
    document.getElementById("testPassword").innerHTML="";
    document.getElementById("testRepassword").innerHTML="";

    const form = document.forms['form'];
    const username=form.username.value;
    const pwd = form.elements.pwd.value;
    const confirm = form.elements.confirm.value;

    var checkLengthuser=validate_Lengthuser(3,15,username);
    var checkLengthpwd=validate_Lengthpwd(6,20,pwd);
    var checkpwd=validatepwd(pwd,confirm,checkLengthpwd);

    if (checkLengthuser  && checkLengthpwd && checkpwd == true){
        console.log("ok");
        $.ajax({
        type: "POST",
        url: "/modifyUserAll",
        dataType: "json",   // 期待后端返回数据的类型
        data: {
            pwd : pwd, name : username
         },
        success: function (res) {//返回数据根据结果进行相应的处理
            console.log('ajax success!')
            var status = JSON.parse(JSON.stringify(res)).status;
            console.log(status);
            if(status==true){

                alert("Successfully modification!");
                window.location.href='/MemberCenter';
            }
        }

})
    }
}

