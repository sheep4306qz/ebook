$(document).ready(function(){
    function set_ddl_date(year_start) {
        var now = new Date();
        var Today=new Date();
        var today_year = Today.getFullYear();
        var today_month = Today.getMonth()+1;
        var today_day = Today.getDate();
        console.log(today_year);
        console.log(today_month);
        console.log(today_day);
        // var obj = document.getElementById("ddl_month");
        
        //年(year_start~今年)
        for (var i = now.getFullYear(); i >= year_start; i--) {
          $('#ddl_year').
          append($("<option></option>").attr("value", i).text(i));
        }
        var year  ;
        var month ;
        
        
        $('#ddl_year').change(onChang_month);
        $('#ddl_month').change(onChang_date);

        function onChang_month() {

            // var year = parseInt($('#ddl_year').val());
            year  = parseInt($('#ddl_year').val());
            
            if (year >=1) {

                
                

                if(year!=today_year){

                    console.log(year);
                    $('#ddl_month option').remove(); 
                    $('#ddl_month').
                        append($("<option></option>").attr("value", 0).text("month")); 
                        $('#ddl_day option').remove(); 
                        $('#ddl_day').
                            append($("<option></option>").attr("value", 0).text("day")); 
                    for (var i = 1; i <= 12; i++) {
                        $('#ddl_month').
                        append($("<option></option>").attr("value", i).text(i));
                      }    

                }
                
            else if(year==today_year){

                    console.log(year);
                    $('#ddl_month option').remove(); 
                    $('#ddl_month').
                        append($("<option></option>").attr("value", 0).text("month")); 
                        $('#ddl_day option').remove(); 
                        $('#ddl_day').
                            append($("<option></option>").attr("value", 0).text("day")); 
                   
                    for (var i = 1; i <= today_month; i++) {
                        $('#ddl_month').
                        append($("<option></option>").attr("value", i).text(i));
                      }
    
                }
                // else if($('#ddl_year').val() == " " ){
                //     $('#ddl_month option').remove(); 
                //     $('#ddl_month').
                //         append($("<option></option>").attr("value", " ").text("month")); 
                //         $('#ddl_day option').remove(); 
                //         $('#ddl_day').
                //             append($("<option></option>").attr("value", " ").text("day")); 
    
                // }
    

            }else if(year == 0 ){
                $('#ddl_month option').remove(); 
                    $('#ddl_month').
                        append($("<option></option>").attr("value", 0).text("month")); 
                        $('#ddl_day option').remove(); 
                        $('#ddl_day').
                            append($("<option></option>").attr("value", 0).text("day")); 
                   
              }

        }


        var date_temp;

        function onChang_date() {

            month  = parseInt($('#ddl_month').val());

            if ( year>=1 && month >= 1) {
        
            //   var month = parseInt($('#ddl_month').val());
                

              if(year==today_year && month==today_month){

                date_temp = new Date($('#ddl_year').val(), $('#ddl_month').val(), 0);
                // $("#ddl_day option").each(function() {

                //     if ($(this).val() != -1 && $(this).val() > date_temp.getDate()) {
                //         $(this).remove();
                //     }
                //   });

                $('#ddl_day option').remove(); 
                    $('#ddl_day').
                        append($("<option></option>").attr("value", 0).text("day")); 

                  for (var i = 1; i <= today_day; i++) {
                    if (!$("#ddl_day option[value='" + i + "']").length) {
                      $('#ddl_day').
                      append($("<option></option>").attr("value", i).text(i));
                    }
                  }

              }else{

              date_temp = new Date($('#ddl_year').val(), $('#ddl_month').val(), 0);
        
              $('#ddl_day option').remove(); 
              $('#ddl_day').
                  append($("<option></option>").attr("value", 0).text("day")); 
        
              //加入此月份的天數
              for (var i = 1; i <= date_temp.getDate(); i++) {
                if (!$("#ddl_day option[value='" + i + "']").length) {
                  $('#ddl_day').
                  append($("<option></option>").attr("value", i).text(i));
                }
              }


            } 
        }else if(month == 0 ){
            
                        $('#ddl_day option').remove(); 
                        $('#ddl_day').
                            append($("<option></option>").attr("value", 0).text("day")); 
                   
              }
          }
        

       
       
      }
    
      //1911為開始年份
      set_ddl_date(1911);




})
    
    // email 格式確認
    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
            document.getElementById("testEmail").innerHTML="<font color='red'>* Enter a valid email!</font>"
            $("[name='email']").focus(); //focus那個input
            return false;
        }else{
            return true;
        }
        }


    //gender 是否有選 
    function IsCheckGender(gender){
       
        for (i=0; i<gender.length; i++)
		if (gender[i].checked ){
           return true;
        }
        
        document.getElementById("testGender").innerHTML="<font color='red'>* Please check a gender!</font>"
        $("[name='gender']").focus(); //focus那個input
        return false;
        
    }    
    

    // birth year
    function IsCheckBirthYear(birth){
        
		if (birth =="0"){
			document.getElementById("testBirth").innerHTML="<font color='red'>* Please make a selection!</font>"
            $("#ddl_year").focus(); //focus那個input
            return false;
        }
        else{
            return true;
        }
    }    


    // birth month
    function IsCheckBirthMonth(birth){
        
		if (birth =="0"){
			document.getElementById("testBirth").innerHTML="<font color='red'>* Please make a selection!</font>"
            $("#ddl_month").focus(); //focus那個input
            return false;
        }
        else{
            return true;
        }
    }
    

    // birth day
    function IsCheckBirthDay(birth){
        
		if (birth ==0){
			document.getElementById("testBirth").innerHTML="<font color='red'>* Please make a selection!</font>"
            $("#ddl_day").focus(); //focus那個input
            return false;
        }
        else{
            return true;
        }
    }

        
    // name 長度限制
    function validate_Lengthuser(minLegth,maxlength,inputFiled)
    {
    if(inputFiled.length<minLegth||inputFiled.length>maxlength)
    {
        
        document.getElementById("testName").innerHTML="<font color='red'>* Name shall be 3-15 characters!</font>"
        $("[name='username']").focus(); //focus那個input
        return false;
    }else{
        return true;
    }
    }


    // password 長度限制
    function validate_Lengthpwd(minLegth,maxlength,inputFiled)
    {
    if(inputFiled.length<minLegth||inputFiled.length>maxlength)
    {
        document.getElementById("testPassword").innerHTML="<font color='red'>* Password shall be 6-20 characters!</font>"
        $("[name='pwd']").focus(); //focus那個input
        return false;
    }else{
        return true;
    }

    }
    function validatepwdchar(inputFiled){
        var regpwd = /^[a-zA-Z0-9]+$/;
        var inputFiled=inputFiled;
        if(!regpwd.test(inputFiled) && inputFiled!=""){
            document.getElementById("testPassword").innerHTML="<font color='red'>* Please don't fill in special characters!</font>"
            $("[name='pwd']").focus(); //focus那個input
            return false;
        }else{
            return true;
        }
        }

    // password 驗證
    function validatepwd(pwd,confirm,checkLengthpwd) {
    if(pwd != confirm && checkLengthpwd==true)
    {
        document.getElementById("testRepassword").innerHTML="<font color='red'>* Different from password!</font>"
        $("[name='confirm']").focus(); //focus那個input
        return false;
    }
    else {
        return true;
    }
    }



    // onclick 
    function ShowMeDate() {
    document.getElementById("testName").innerHTML="";
    document.getElementById("testEmail").innerHTML="";
    document.getElementById("testPassword").innerHTML="";
    document.getElementById("testRepassword").innerHTML="";
    document.getElementById("testGender").innerHTML="";
    document.getElementById("testBirth").innerHTML="";
    document.getElementsByClassName("info").innerHTML="";

    const form = document.forms['form'];
    const username=form.elements.username.value
    const email = form.elements.email.value;
    const ddl_year = form.elements.ddl_year.value;
    const ddl_month = form.elements.ddl_month.value;
    const ddl_day = form.elements.ddl_day.value;
    const gender = form.elements.gender.value;
    const pwd = form.elements.pwd.value;
    const confirm = form.elements.confirm.value;

    var checkLengthuser=validate_Lengthuser(3,15,username);
    var checkEmail=IsEmail(email);
    var checkGender=IsCheckGender(form.gender);
    var checkYear = IsCheckBirthYear(ddl_year);
    var checkMonth = IsCheckBirthMonth(ddl_month);
    var checkDay = IsCheckBirthDay(ddl_day);
    var checkLengthpwd=validate_Lengthpwd(6,20,pwd);
    var checkpwd=validatepwd(pwd,confirm,checkLengthpwd);
    var checkpwdchar=validatepwdchar(pwd);

    if (checkLengthuser && checkEmail && checkLengthpwd && checkpwd &&checkGender &&checkYear&&checkMonth &&checkDay &&checkpwdchar){
        console.log("ok");
        $.ajax({
        type: "POST",
        url: "/signUp",
        dataType: "json",   // 期待后端返回数据的类型
        data: {
            email : email, pwd : pwd, name : username, 
            ddl_year : ddl_year, ddl_month : ddl_month, 
            ddl_day : ddl_day, gender : gender
         },
        success: function (res) {//返回数据根据结果进行相应的处理
            console.log('ajax success!')
            var status = JSON.parse(JSON.stringify(res)).status
            var note = JSON.parse(JSON.stringify(res)).note

            console.log(status)
            console.log(note)

            if(status==false){
                if(note==false){
                    $('.infoAccount').text("* This account is existed!").css('color','red');
                }
                else if(note==true){
                alert("Please rewrite the information!");
                window.location.href='/Member_Register';
            }
            }else{
                alert("Successfully registered! Please login. ");
                window.location.href='/Member_login';
            }
            // console.log(typeof(JSON.parse(JSON.stringify(res)).status))
            // console.log(JSON.parse(JSON.stringify(res)).duplicate)
        },
        error: function () {
            console.log('ajax error!')
        }
        });
    }
  }
  