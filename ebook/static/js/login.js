

    
    // email 格式確認
    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
            document.getElementById("testEmail").innerHTML="<font color='red'>* Enter a valid email!</font>"
            $("[name='email']").focus(); //focus那個input
            return false;
        }else{
            return true;
        }
        }


    

    // password 長度限制
    function validate_Lengthpwd(minLegth,maxlength,inputFiled)
    {
    if(inputFiled.length<minLegth||inputFiled.length>maxlength)
    {
        document.getElementById("testPassword").innerHTML="<font color='red'>* Password shall be 6-20 characters!</font>"
        $("[name='pwd']").focus(); //focus那個input
        return false;
    }else{
        return true;
    }
    }

    



    // onclick 
    function ShowMeDate() {
    document.getElementById("testEmail").innerHTML="";
    document.getElementById("testPassword").innerHTML="";
    // document.getElementsByClassName("info").innerHTML="";

    const form = document.forms['form'];
    const email = form.elements.email.value; 
    const pwd = form.elements.pwd.value;
   

    
    var checkEmail=IsEmail(email);
    var checkLengthpwd=validate_Lengthpwd(6,20,pwd);

    if (checkEmail && checkLengthpwd== true){
        console.log("ok");
        $.ajax({
        type: "POST",
        url: "/login",
        dataType: "json",   // 期待后端返回数据的类型
        data:{
            email : email, pwd : pwd
        },
        success: function (res) {//返回数据根据结果进行相应的处理
            console.log('ajax success!')
            var status = JSON.parse(JSON.stringify(res)).status
            var note =JSON.parse(JSON.stringify(res)).note

            console.log(status)
            console.log(note)

            if(status==false){
                if(note==true){
                    $('.infoAccount').text("* This account doesn't exist!").css('color','red');
                }
                else if(note==false)
                $('.infoPassword').text("* Wrong password!").css('color','red');
            }
            else{
                alert("Successful login!");
                window.location.href='/';
            }
            // console.log(typeof(JSON.parse(JSON.stringify(res)).status))
            // console.log(JSON.parse(JSON.stringify(res)).duplicate)
            
        },
        error: function () {
            console.log('ajax error!')
        }
        });
    }
  }
  